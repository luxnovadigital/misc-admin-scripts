# Miscellaneous snippets created from time to time

* [bootstrap-debian](https://bitbucket.org/luxnovadigital/misc-admin-scripts/raw/master/bootstrap-debian): Script to bootstrap Debian distribution
* [update-route53](https://bitbucket.org/luxnovadigital/misc-admin-scripts/raw/master/update-route53):
A LSB-compliant init.d script to update hostname and Route53 A record with the value specified by EC2 Name tag. As Route53 doesn't support dynamic PTR record creation. CNAME approach is not really necessary.
* [update-postfix-with-new-hostname](https://bitbucket.org/luxnovadigital/misc-admin-scripts/raw/master/update-postfix-with-new-hostname): A bash shell script to update hostname relevant settings for Postfix. This script should be executed only once after new instance creation based on my baseline AMI.
* [mongod.service](https://bitbucket.org/luxnovadigital/misc-admin-scripts/raw/master/mongod.service): MongoDB 3.0.3 for Ubuntu/Debian utilizes Upstart to handle service daemonization, but Ubuntu 15.04 (Vivid) uses systemd by default. When /etc/init.d/mongod, the error message ````
initctl: Unable to connect to Upstart: Failed to connect to socket /com/ubuntu/upstart: Connection refused```` shows up and nothing happened. Switching back to upstart-sysv just because mongodb doesn't support it is kinda weird. This handcraft file is to start/stop mongodb 3.0.3 on Ubuntu Vivid.
